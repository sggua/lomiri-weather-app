# make the qml files visible on qtcreator
file(GLOB HEAD_STATE_QML_FILES *.qml)

add_custom_target(lomiri-weather-app_HEAD_STATE_QMLFiles ALL SOURCES ${HEAD_STATE_QML_FILES})

install(FILES ${HEAD_STATE_QML_FILES} DESTINATION ${LOMIRI_WEATHER_APP_DIR}/components/HeadState)
